# Ansible ruby-install Role

A role used to install `ruby-install` from source.

## Installation

Install the role with:

```
git clone https://gitlab.com/czerasz/ansible-ruby-install.git /etc/ansible/roles/czerasz.ruby-install
```

## Usage

```
---

- name: Install ruby-install
  hosts: all
  roles:
    - role: czerasz.ruby-install
      vars:
        version: <version>
        download_checksum: "sha256:<checksum>"
        signature_checksum: "sha256:<checksum>"
```

Or for version `0.6.1`:

```
---

- name: Install ruby-install
  hosts: all
  roles:
    - role: czerasz.ruby-install
      vars:
        version: 0.6.1
```

## Role Variables

| name | description | default |
| --- | --- | --- |
| `version` | `ruby-install` version | `0.6.1` |
| `download_checksum` | The `sha256` checksum of the downloaded archive file. Get the checksum with: `curl -Ls https://github.com/postmodern/ruby-install/archive/v<version>.tar.gz | sha256sum | sed 's/\(.*\)\s\s\-/\1/'` | `sha256:b3adf199f8cd8f8d4a6176ab605db9ddd8521df8dbb2212f58f7b8273ed85e73` |
| `signature_checksum` | The `sha256` checksum of the downloaded signature file. Get the checksum with: `curl -Ls https://raw.github.com/postmodern/ruby-install/master/pkg/ruby-install-<version>.tar.gz.asc | sha256sum | sed 's/\(.*\)\s\s\-/\1/'` | `sha256:e12eb63c00f5288d23d97d71f2086f9479a8985aed891836d15489e0ec67aae6` |
| `sign_key_file_checksum` | The `sha256` checksum of the downloaded signature file | `sha256:cd66de33ae91d6d537cba2e73e2d45a29c3d7efdb117fd93aaf3b907fcac86c7` |
| `sign_gpg_key` | GPG key ID used to sign the checksum file | `B9515E77` |
| `sign_gpg_key_fingerprint` | GPG fingerprint of the key used to sign the checksum file | `04B2F3EA654140BCC7DA1B5754C3D9E9B9515E77` |

# Test

Requirements:

- [Docker Compose](https://docs.docker.com/compose/)

Run tests with:

```
docker-compose -f test/docker-compose.yml up
```
