#!/usr/bin/env bats

if [ -z $ruby_install_version ]; then
  echo "Error: can't run tests"
  echo "Please provide the ruby_install_version environment variable"
  exit 1
fi

@test "ruby-install binary is found in $PATH" {
  run which ruby-install

  [ "$status" -eq 0 ]
}

@test "ruby-install is installed in version ${ruby_install_version}" {
  run /usr/local/bin/ruby-install --version

  version=$(echo "${output}" | sed "s/.*\([0-9]\+\.[0-9]\+\.[0-9]\+\)/\1/")

  echo "DEBUG: current version: ${version}"
  echo "DEBUG: expected version: ${ruby_install_version}"

  [ "${version}" == "${ruby_install_version}" ]
}
